cmake_minimum_required( VERSION 3.1.0 )

project( date_prj )

include( GNUInstallDirs )

find_package( Threads REQUIRED )

# Override by setting on CMake command line.
set( CMAKE_CXX_STANDARD 17 CACHE STRING "The C++ standard whose features are requested.")

option( USE_SYSTEM_TZ_DB "Use the operating system's timezone database" OFF )
option( USE_TZ_DB_IN_DOT "Save the timezone database in the current folder" OFF )
option( BUILD_SHARED_LIBS  "Build a shared version of library" OFF )
option( ENABLE_DATE_TESTING "Enable unit tests" ON )
option( DISABLE_STRING_VIEW "Disable string view" OFF )

function( print_option OPT )
	if ( NOT DEFINED PRINT_OPTION_CURR_${OPT} OR ( NOT PRINT_OPTION_CURR_${OPT} STREQUAL ${OPT} ) )
		set( PRINT_OPTION_CURR_${OPT} ${${OPT}} CACHE BOOL "" )
		mark_as_advanced(PRINT_OPTION_CURR_${OPT})
		message( "# date: ${OPT} ${${OPT}}" )
	endif( )
endfunction( )

print_option( BUILD_SHARED_LIBS  )

set( HEADER_FOLDER "include" )

# This is needed so IDE's live MSVC show header files
set( HEADER_FILES
	${HEADER_FOLDER}/date/date.h
)

if( BUILD_SHARED_LIBS )
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -fPIC")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -fPIC")
endif( )

add_library(date_interface INTERFACE) # an interface (not a library), to enable automatic include_directory (for when just date.h, but not "tz.h and its lib" are needed)

# add include folders to the INTERFACE and targets that consume it
target_include_directories(date_interface INTERFACE
    "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>"
    $<INSTALL_INTERFACE:include>
)

if(WIN32 AND NOT CYGWIN)
    set(DEF_INSTALL_CMAKE_DIR CMake)
else()
    set(DEF_INSTALL_CMAKE_DIR ${CMAKE_INSTALL_LIBDIR}/cmake/date)
endif()

install( TARGETS date_interface EXPORT dateConfig )
install( EXPORT dateConfig DESTINATION ${DEF_INSTALL_CMAKE_DIR} )
install( DIRECTORY ${HEADER_FOLDER}/ DESTINATION include/ )
